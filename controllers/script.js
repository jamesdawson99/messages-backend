const _ = require('lodash');
const { ObjectId } = require('mongodb');
const { Script } = require('./../models/script');
const { Children } = require('./../models/children');
const { Settings } = require('./../models/settings');
/**
 * @api {post} /v1/Scripts           Create new script template
 * @apiName Create script
 * @apiVersion 1.0.0
 * @apiGroup script
 * 
 * @apiParam {String} name          script template's name
 * @apiParam {String} description   script template's description (optional)
 * @apiParam {Boolean} singleton    script template's singleton
 * @apiParam {Boolean} freeTypes    script template's free typed
 * @apiParam {String} subject       script's subject
 * @apiParam {String} from          script's from field
 * @apiParam {String} replyTo       script's reply to field
 * @apiParam {String} text          script's text content
 * @apiParam {String} html          script's html content
 * 
 * @apiSuccess (Success 2xx) 201/Created
 * @apiSuccessExample Success-Response:
 *  HTTP 201 CREATED
 *  {
 *      code: 201,
 *      status: 'success',
 *      message: 'Template successfully created'
 *  }
 * @apiError 400/Bad-Request        Invalid Params
 */
exports.create = async (req, res) => {
    const body = _.pick(req.body, [
        'name', 'description', 'author', 'user_id', 'body', 'singleton', 'freetype', 'from'
    ]);
    console.log(body);
    try {
        const script = new Script(body);
        await script.save();

        res.status(201).send({ code: 201, status: 'success', message: 'Template successfully created' });
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};


/**
 * @api {put} /v1/script/:id        Update an script template
 * @apiName UpdateScript
 * @apiVersion 1.0.0
 * @apiGroup script
 * 
 * @apiParam {String} id            script template's id
 * @apiParam {String} name          script template's name
 * @apiParam {String} description   script template's description (optional)
 * @apiParam {Boolean} singleton    script template's singleton
 * @apiParam {Boolean} freeTypes    script template's free typed
 * @apiParam {String} subject       script's subject
 * @apiParam {String} from          script's from field
 * @apiParam {String} replyTo       script's reply to field
 * @apiParam {String} text          script's text content
 * @apiParam {String} html          script's html content
 * 
 * @apiSuccess (Success 2xx) 200/OK
 * @apiSuccessExample Success-Response:
 *  HTTP 200 OK
 *  {
 *      code: 200,
 *      status: 'success',
 *      message: 'Template successfully updated'
 *  }
 * @apiError 400/Bad-Request        Invalid Params
 * @apiError 404/Not-Found          Not Found
 */
exports.update = async (req, res) => {

    const id = req.params.id;
    const body = _.pick(req.body, [
        'name', 'description', 'author', 'user_id', 'body', 'singleton', 'freetype', 'version', 'from' 
    ]);

    if (!ObjectId.isValid(id)) {
        return res.status(400).send({ code: 400, status: 'error', message: 'script id is not valid' });
    }
    console.log(body);
    try {

        // Get master template by id
        const script = await Script.findById(id);

        if (!script) {
            return res.status(404).send({ code: 404, status: 'error', message: 'Script not found' });
        }

        const newChild = _.pick(script, [
            'name', 'description', 'author', 'user_id', 'body', 'singleton', 'freetype', 'version', 'from'
        ]);

        // get current template data and create a new child
        const children = new Script(newChild);
        children.isChild = true;
        await children.save();

        // Create new script version
        await Script.findByIdAndUpdate(id, {
            $set: body,
            $inc: { version: 1 },
            $push: { childrens: children._id }
        });
            

        res.status(200).send({ code: 200, status: 'success', message: 'script successfully updated' });
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};

/**
 * @api {get} /v1/script/:filter    Get all script templates
 * @apiName Getscript
 * @apiVersion 1.0.0
 * @apiGroup script
 * 
 * @apiParam {String} filter        active || archived
 * 
 * @apiSuccess (Success 2xx) 200/OK
 * @apiSuccessExample Success-Response:
 *  HTTP 200 OK
 *  {
 *      code: 200,
 *      status: 'success',
 *      script: Array
 *  }
 * @apiError 400/Bad-Request        Invalid Params
 */
exports.getAll = async (req, res) => {
    const filter = req.params.filter;
    const page = req.params.page;
    const archived = filter === 'active' ? false : true;
    try {
        let skip = 0;
        if (page !== '1'){
             skip = (page * 10) - 10;
        } 
        console.log(page, skip);
        const script = await Script
                    .find({archived, isChild : false})
                    .skip(parseInt(skip))
                    .limit(10)
                    .then((script) => {
                      res.status(200).send({ code: 200, status: 'success', script });
                    }).catch((error) => {
                        console.error(error);
                         res.status(400).send({ code: 400, status: 'error', message: err });
                    });
      
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};
exports.heartbeat = async(req, res) => {
    res.status(200).send({ code: 200});
}
exports.countAll = async (req, res) => {
    const filter = req.params.filter;
    const archived = filter === 'active' ? false : true;
    try {
        const count = await Script.count({archived, isChild : false});
        res.status(200).send({ code: 200, status: 'success', count });
    } catch (err) {
        res.status(400).send({ code: 400, status: 'error', message: err });
    }   
};
exports.getSettings = async (req, res) => {
    try {
        const settings = await Settings.find();
        
        res.status(200).send({ code: 200, status: 'success', settings });
    } catch (err) {
        res.status(400).send({ code: 400, status: 'error', message: err });
    }

};
exports.updateSettings = async (req, res) => {
    const body = _.pick(req.body, [
        'mobile'
    ]);
    console.log('Test 1');
    //if ID exists find and update else create new settings collection
    try {
        if(req.params.id) {
            console.log('Test 1', req.params.id);
            
            const id = req.params.id;
            
            if (!ObjectId.isValid(id)) {
                return res.status(400).send({ code: 400, status: 'error', message: 'script id is not valid' });
            }
            await Settings.findByIdAndUpdate(id, {
                $set: body
            }, { runValidators: true, context: 'query' });
        } else {
            console.log(body);
            const settings = new Settings(body);
            await settings.save();
        }

        res.status(200).send({ code: 200, status: 'success', message: 'script successfully updated' });
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};
/**
 * @api {get} /v1/Scripts/:id        Get an script template
 * @apiName Getscript
 * @apiVersion 1.0.0
 * @apiGroup Scripts
 * 
 * @apiParam {String} id            script template's id
 * 
 * @apiSuccess (Success 2xx) 200/OK
 * @apiSuccessExample Success-Response:
 *  HTTP 200 OK
 *  {
 *      code: 200,
 *      status: 'success',
 *      script: Object
 *  }
 * @apiError 400/Bad-Request        Invalid Params
 * @apiError 404/Not-Found          Not Found
 */
exports.getOne = async (req, res) => {
    const id = req.params.id;

    console.log(id);
    if (!ObjectId.isValid(id)) {
        return res.status(400).send({ code: 400, status: 'error', message: 'templates id is not valid' });
    }

    try {
        const script = await Script.findById(id).populate('childrens');

        if (!script) {
            return res.status(404).send({ code: 404, status: 'error', message: 'script not found' });
        }

        res.status(200).send({ code: 200, status: 'success', script });
    } catch (err) {
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};

/**
 * @api {patch} /v1/script/:id          Archive/Unarchive an script template
 * @apiName Archivescript
 * @apiVersion 1.0.0
 * @apiGroup script
 * 
 * @apiParam {String} id                script template's id
 * 
 * @apiSuccess (Success 2xx) 200/OK
 * @apiSuccessExample Success-Response:
 *  HTTP 200 OK
 *  {
 *      code: 200,
 *      status: 'success',
 *      message: 'script successfully updated'
 *  }
 * @apiError 400/Bad-Request        Invalid Params
 * @apiError 404/Not-Found          Not Found
 */
exports.archive = async (req, res) => {
    const id = req.params.id;

    if (!ObjectId.isValid(id)) {
        return res.status(400).send({ code: 400, status: 'error', message: 'script id is not valid' });
    }

    try {
        const script = await Script.findById(id);

        if (!script) {
            return res.status(404).send({ code: 404, status: 'error', message: 'script not found' });
        }

        script.archived = !script.archived;
        console.log('test here', script);
        await script.save();

        res.status(200).send({ code: 200, status: 'success', message: 'script successfully updated', script });
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
};

exports.duplicate = async (req, res) => {
    const id = req.params.id;

    if (!ObjectId.isValid(id)) {
        return res.status(400).send({ code: 400, status: 'error', message: 'script id is not valid' });
    }

    try {
        const script = await Script.findById(id);

        if (!script) {
            return res.status(404).send({ code: 404, status: 'error', message: 'script not found' });
        }

        const duplicate = _.pick(script, [
            'name', 'description', 'body', 'singleton', 'freetype', 'version', 'from'
        ]);

        // get current template data and duplicate it into a new record
        const duplicateScript = new Script(duplicate);
        await duplicateScript.save();

        res.status(200).send({ code: 200, status: 'success', message: 'script successfully duplicated', script });
    } catch (err) {
        console.log(err);
        res.status(400).send({ code: 400, status: 'error', message: err });
    }
}
