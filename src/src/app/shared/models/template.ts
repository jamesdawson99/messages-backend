import { Template } from './template';


export interface Template {
    _id: string;
    name: string;
    body: string;
    description: string;
    version: number;
    archived: boolean;
    childrens: Array<Template>;
    isChild: boolean;
    created_at: string;
    updated_at: string;
}
