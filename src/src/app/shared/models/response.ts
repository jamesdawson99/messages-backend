export interface Response {
    outputString: string;
    error: boolean;
}

