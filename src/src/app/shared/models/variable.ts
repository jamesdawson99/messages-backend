export class Variable {
    public name: string;
    public tooltip: string;
    public var_string: string;

    constructor(name: string, tooltip: string, var_string: string) {
      this.name = name;
      this.tooltip = tooltip;
      this.var_string = var_string;
    }
}
