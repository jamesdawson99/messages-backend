export interface User {
    username: string;
    _id: string;
    password: string;
    firstName: string;
    lastName: string;
    roles: string[];
    skills: string[];
    active: boolean;

}
