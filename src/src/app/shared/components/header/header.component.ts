import { TemplateService } from './../../../services/template.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testheader',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private templateService: TemplateService) { }

  ngOnInit() {
    this.templateService.getUser().subscribe(response => {
    });
  }

}
