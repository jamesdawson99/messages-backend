import { MessageComponent } from './components/message/message.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplatesComponent } from './components/templates/templates.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'app/script', children: [
    { path: '', redirectTo: 'templates', pathMatch: 'full' },
    { path: 'templates', component: TemplatesComponent },
    { path: 'templates/script', component: MessageComponent },
    { path: 'templates/script/:id', component: MessageComponent },
    { path: '404', component: PageNotFoundComponent },
    { path: '**', redirectTo: '404' }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
