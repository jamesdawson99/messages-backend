import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Scripts';
  user = 'James';
  serverRoutes = [
    {name: 'Collections-Hub', href: '/'},
    {name: 'Letter Builder', href: '/app/letter'},
    {name: 'SMS Builder', href: '/app/sms'},
    {name: 'Email builder', href: '/app/email'},
  ];
  clientRoutes = [
    {name: 'Templates', href: '/app/script/templates'}
  ];

}
