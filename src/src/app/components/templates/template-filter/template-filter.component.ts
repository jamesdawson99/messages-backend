import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-template-filter',
  templateUrl: './template-filter.component.html',
  styleUrls: ['./template-filter.component.scss']
})
export class TemplateFilterComponent implements OnInit {
  @Output() onStateChange = new EventEmitter<any>();
  public state: string;

  constructor() {
    this.state = 'active';
  }

  ngOnInit() {
  }

  handleChange() {
    this.onStateChange.emit({ state: this.state });
  }
}
