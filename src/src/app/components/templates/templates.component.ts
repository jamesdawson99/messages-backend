import { TemplateService } from './../../services/template.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss']
})
export class TemplatesComponent implements OnInit, OnDestroy {

  constructor(
    private templateService: TemplateService,
    private toastyService: ToastyService
  ) { }
  private ngUnsubscribe: any = new Subject();
  public state: any;
  public page: number;
  public total: number;
  public subscribers: any = {};
  public templates: Array<any> = [];
  ngOnInit() {
    this.page = 1;
    this.state = 'active';
    this.getTemplates(this.state, this.page);
    this.templateService
    .countTemplate(this.state)
    .takeUntil(this.ngUnsubscribe)
    .subscribe(response => {
      this.total =  response.count;
    });
  }

  setState(event: any) {
    this.state = event.state;
    this.getTemplates(this.state, this.page);
    this.templateService
    .countTemplate(this.state)
    .takeUntil(this.ngUnsubscribe)
    .subscribe(response => {
      this.total =  response.count;
      console.log(this.total);
    });
  }
  getTemplates(state: string, page: number) {
    this.templateService
    .getTemplates(state, page)
    .takeUntil(this.ngUnsubscribe)
    .subscribe(response => {
      this.templates = response.script;
    });
  }
  pageChange(page) {
    this.templateService
    .getTemplates(this.state, page)
    .takeUntil(this.ngUnsubscribe)
    .subscribe(response => {
      this.templates = response.script;
    });
  }
  refreshData() {
    this.getTemplates('active', 1);
  }
  archiveTemplate(event: any) {
    this.templateService
    .archiveTemplate(event.id)
    .takeUntil(this.ngUnsubscribe)
    .subscribe((response) => {
      this.toastyService.success(
        { title: response.script.archive ? 'Successfully Activated ' + response.script.name : 'Successfully Archived ' + response.script.name}
      );
      this.getTemplates(this.state, this.page);

    }, (err) => {
      this.toastyService.error({ title: err.message });
    });
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
