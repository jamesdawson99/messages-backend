import { ToastyService } from 'ng2-toasty';
import { TemplateService } from './../../../services/template.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import { Template } from '../../../shared/models/template';
import * as _ from 'lodash';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'app-template-table',
  templateUrl: './template-table.component.html',
  styleUrls: ['./template-table.component.scss']
})
export class TemplateTableComponent implements OnInit, OnDestroy {
  @Input() inputTemplates: Array<any> = [];
  @Input() total: number;
  @Output() onArchiveClick = new EventEmitter<any>();
  @Output() refreshData = new EventEmitter<any>();
  @Output() handlePageClick = new EventEmitter<any>();
  public p: number;
  public state: string;
  public version: number;
  public subscribers: any = {};
  private ngUnsubscribe: any = new Subject();
  constructor(private templateService: TemplateService,
              private toastyService: ToastyService) { }

  ngOnInit() {
    this.version = 0;
    this.p = 1;
  }
  onVersionClick(version: number) {
    this.version = version;
  }

  pageChanged(event) {
    this.p = event;
    this.handlePageClick.emit(event);
  }
  duplicate(id: string) {
    this.templateService
    .duplicateTemplate(id)
    .takeUntil(this.ngUnsubscribe)
    .subscribe((response) => {
      this.refreshData.emit();
      this.toastyService.success(
        { title: 'Successfully Duplicated ' + response.script.name}
      );
    }, (err) => {
      this.toastyService.error({ title: err.message });
    });
  }
  handleArchive(id: string) {
    this.templateService
    .getTemplate(id)
    .takeUntil(this.ngUnsubscribe)
    .subscribe((response) => {
    });
    this.onArchiveClick.emit({ id });
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
