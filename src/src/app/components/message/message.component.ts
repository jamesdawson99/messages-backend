import { Component, OnInit, OnDestroy } from '@angular/core';
import { Templates } from './../../shared/models/templates';
import { Template } from './../../shared/models/template';
import { TemplateService } from './../../services/template.service';
import {ActivatedRoute } from '@angular/router';
import { Response } from '../../shared/models/response';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnDestroy {
  public   template: string;
  public   previewedTemplate: any;
  public   chars: number;
  public   smsCount: number;
  public   isChild: boolean;
  public   id: number;
  public   name: string;
  public   description: string;
  public   archived: boolean;
  public   newTemplate: boolean;
  public   templates: Template;
  public   interpolateResponse: string;
  public   version: number;
  public   childrens: Array<Template>;
  public   settingsID: boolean;
  private ngUnsubscribe: any = new Subject();

  variables = {
    originalBalance: 307.89,
    originalBalanceDate: Date.now(),
    clientName : 'Sarah Jessica Smith',
    originalReference: '3001',
    customerPhoneNumber: '07785647372',
    originalCreditor: 'Creditors name',
    firstName: 'Sarah',
    lastName: 'Smith',
    assignmentDate: Date.now(),
    instructDate: Date.now(),
    onlinePin: '9811',
    customerReference: '3001',
    customerReference1: '2989',
    customerEmail: 'sarahsmith@domain.com',
    title: 'Ms',
    currentBalance: 408.90,
    totalAccounts: 3
  };

  constructor(
    private templateService: TemplateService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.newTemplate = true;
    this.version = 1;
    this.route.params
    .takeUntil(this.ngUnsubscribe)
    .subscribe(params => {
      this.id = params.id;
      this.version = params.item;
    });

    if (this.id ) {
      this.templateService.getTemplate(this.id.toString())
      .takeUntil(this.ngUnsubscribe)
      .subscribe((response: Templates) => {
        const script = response.script;
        this.template = script.body;
        this.name = script.name;
        this.description = script.description;
        this.isChild = script.isChild;
        this.version = script.version;
        this.archived = script.archived;
        this.childrens = response.script.childrens;
        this.childrens.push(response.script);
        this.stringInterpolate(this.template);
        this.newTemplate = false;
      });
    }
  }
  setVersion(template: Template) {
   console.log(template);
   this.template = template.body;
   this.stringInterpolate(this.template);
   this.name = template.name;
   this.description = template.description;
  }
  setTemplate(template: string) {
      this.template = template;
      this.stringInterpolate(template);
  }
  stringInterpolate(temp: string) {
    this.templateService.interpolateTemplate(temp, this.variables)
    .takeUntil(this.ngUnsubscribe)
    .subscribe((response: Response) => {
      this.previewedTemplate = response.outputString;
      console.log(this.previewedTemplate);
      this.chars = this.countTemplate(this.previewedTemplate);
      this.smsCount = this.smsCounter(this.chars);
    });
  }
  countTemplate(temp: string) {
    return temp.length;
  }
  smsCounter(chars: number) {
    let sms_count = Math.ceil(chars / 160);
    if (sms_count === 0) { sms_count = 1; }
    return sms_count;
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
