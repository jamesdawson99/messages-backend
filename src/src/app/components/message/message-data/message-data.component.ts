import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message-data',
  templateUrl: './message-data.component.html',
  styleUrls: ['./message-data.component.scss']
})
export class MessageDataComponent implements OnInit {

  @Input() newMessageCount: number;
  @Input() newChars: number;
  constructor() { }

  ngOnInit() {
    this.newChars = 0;
    this.newMessageCount = 1;
  }

}
