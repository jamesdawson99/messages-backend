import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageVersionComponent } from './message-version.component';

describe('MessageVersionComponent', () => {
  let component: MessageVersionComponent;
  let fixture: ComponentFixture<MessageVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
