import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Template } from './../../../shared/models/template';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-message-version',
  templateUrl: './message-version.component.html',
  styleUrls: ['./message-version.component.scss']
})
export class MessageVersionComponent implements OnInit, OnChanges {
  selectedVersion: number;
  @Input() children: Array<Template>;
  @Input() version: number;
  @Input() newTemplate: boolean;
  @Output() newVersion: EventEmitter<Template> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.selectedVersion = -1;
  }
  ngOnChanges() {
    this.selectedVersion = this.version;
  }
  versionChanged() {
    this.newVersion.emit(this.children[this.selectedVersion - 1]);
  }
}
