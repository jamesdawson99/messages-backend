import { SafeHtmlPipe } from './../../../shared/pipes/safeHtml.pipe';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message-preview',
  templateUrl: './message-preview.component.html',
  styleUrls: ['./message-preview.component.scss']
})
export class MessagePreviewComponent implements OnInit {
  template: string;
  @Input() newTemplate: string;

  constructor() { }

  ngOnInit() {
    if (!this.newTemplate) {
      this.newTemplate = 'Message template preview.';
    }this.template = this.newTemplate;
  }
}
