import { environment } from './../../../../environments/environment';
import { Router } from '@angular/router';
import { TemplateService } from './../../../services/template.service';
import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { Template } from '../../../shared/models/template';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'app-message-create',
  templateUrl: './message-create.component.html',
  styleUrls: ['./message-create.component.scss']
})
export class MessageCreateComponent implements OnInit, OnDestroy {
  @Input() template: string;
  @Input() name: string;
  @Input() description: string;
  @Input() singleton: boolean;
  @Input() body: string;
  @Input() freetype: boolean;
  @Input() newTemplate: boolean;
  @Input() id: string;
  @Input() archived: boolean;
  @Input() isChild: boolean;
  @Input() mobile: string;
  private route: string = environment.apiUrlV1;
  private ngUnsubscribe: any = new Subject();

  constructor(private templateService: TemplateService,
              private router: Router,
              private toastyService: ToastyService) { }

  ngOnInit() {
 }

  saveTemplate() {
    console.log(this.mobile);
    if (this.newTemplate) {
      this.templateService.createTemplate(
        this.name,
        this.description,
        this.templateService.user.username,
        this.templateService.user._id,
        this.body,
        this.singleton,
        this.freetype,
        this.mobile)
        .takeUntil(this.ngUnsubscribe)
        .subscribe((response) => {
          this.toastyService.success({ title: 'Element successfully removed' });
          this.router.navigate([`${this.route}/templates`]);
      }, (err) => {
        this.toastyService.error({ title: err.message });
      });
    } else {
      this.templateService.updateTemplate(
        this.id,
        this.name,
        this.description,
        this.templateService.user.username,
        this.templateService.user._id,
        this.body,
        this.singleton,
        this.freetype,
        this.mobile)
        .takeUntil(this.ngUnsubscribe)
        .subscribe((response) => {
          this.toastyService.success({ title: 'Element successfully removed' });
          this.router.navigate([`${this.route}/templates`]);
      }, (err) => {
        this.toastyService.error({ title: err.message });
      });
    }
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

