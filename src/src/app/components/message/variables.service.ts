import { Variable } from './../../shared/models/variable';
import { EventEmitter, OnInit } from '@angular/core';


export class VariableService {

  variables: Variable[] = [
    new Variable(
      'Original Balance',
      'The original balance of the customers accounts',
      "{{originalBalance | currency: '£'}}"
      ),
    new Variable(
        "Original Balance Date",
        "The original date of the original balance",
        "{{originalBalanceDate | date: 'dd/MM/yyyy'}}"
    ),
    new Variable(
        "Client Name",
        "The client the debt was purchased from",
        "{{clientName}}"
    ),
    new Variable(
        "Original Reference",
        "The original account reference",
        "{{originalReference}}"
    ),
    new Variable(
        "Customer Phone Number",
        "The customer's phone number",
        "{{customerPhoneNumber}}"
    ),
    new Variable(
        "Original Creditor",
        "The original creditor the account was purchased from",
        "{{originalCreditor}}"
    ),
    new Variable(
        "First Name",
        "The customer's first name",
        "{{firstName}}"
    ),
    new Variable(
        "Last Name",
        "The customer's first name",
        "{{lastName}}"
    ),
    new Variable(
        "Assignment Date",
        "The date assigned",
        "{{assignmentDate | date: 'dd/MM/yyyy'}}"
    ),
    new Variable(
        "Instruct Date",
        "The date instructed to collect",
        "{{instructDate | date: 'dd/MM/yyyy'}}"
    ),
    new Variable(
        "Online Pin",
        "The pin number for the customers access to online portal",
        "{{onlinePin}}"
    ),
    new Variable(
        "Customer Reference",
        "The customers reference number",
        "{{customerReference}}"
    ),
    new Variable(
        "Original Customer Reference",
        "The customers original reference number",
        "{{customerReference1}}"
    ),
    new Variable(
        "Customer's Email",
        "The customers email address",
        "{{customerEmail}}"
    ),
    new Variable(
        "Customer's Title",
        "The customers title mr/miss/mrs",
        "{{title}}"
    ),
    new Variable(
        "Current Account Balance",
        "The current balance of account",
        "{{currentBalance | currency: '£'}}"
    ),
    new Variable(
        "Accounts Total",
        "The current balance of all accounts",
        "{{totalAccounts}}"
    ),
  ];
  customerReference1: string;
  varSelected: string;
  variableSelected = new EventEmitter<string>();
  constructor() {}

  setVariableString(var_string: string) {
      this.variableSelected.emit(var_string);
  }

  getVariables(name?: string): any {
    if (name) {
      return this.variables.filter((variable) => variable.name.toLowerCase().includes(name.toLowerCase()));
    }
    return this.variables;
  }
  getVariable(index: number) {
    return this.variables[index];
  }

}
