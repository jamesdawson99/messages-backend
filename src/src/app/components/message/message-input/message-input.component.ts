import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, EventEmitter, Output, Input, OnChanges, OnDestroy } from '@angular/core';
import { VariableService } from './../variables.service';


@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit, OnDestroy, OnChanges {
  public template: string;
  public config: any;
  private ngUnsubscribe: any = new Subject();
  @Input() editTemplate: string;
  @Output() newTemplate: EventEmitter<string> = new EventEmitter();
  constructor(private variableService: VariableService)  { }
  ngOnInit() {
    this.template = '';
    this.variableService.variableSelected
    .takeUntil(this.ngUnsubscribe)
    .subscribe(
      (var_string: any) => {
        console.log(var_string);
        this.template = this.template + var_string;
        this.templateChanged();
      });
    this.config =  {
      contentsCss: 'p{margin:0; padding:0}',
      removeButtons: 'Subscript,Superscript,SpecialChar,Source,Save,Replace,DivContainer',
      toolbarGroups: [
                  { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                  '/',
                  { name: 'insert', groups: [ 'insert' ] },
                  { name: 'basicstyles', groups: [ 'basicstyles' ] },
                  { name: 'paragraph', groups: [ 'list',   'paragraph' ] },
                  { name: 'styles', groups: [ 'styles' ] },
                  { name: 'colors', groups: [ 'colors' ] },
                  ]
    };
  }
  ngOnChanges() {
    this.template = this.editTemplate;
  }
  templateChanged() {
    this.newTemplate.emit(this.template);
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
