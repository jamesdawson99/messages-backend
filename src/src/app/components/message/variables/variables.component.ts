import { Component, OnInit } from '@angular/core';
import { Variable } from './../../../shared/models/variable';
import { VariableService } from '../variables.service';

@Component({
  selector: 'app-variables',
  templateUrl: './variables.component.html',
  styleUrls: ['./variables.component.scss']
})
export class VariablesComponent implements OnInit {

  limit: number;
  variables: Variable[];
  varsLength: number;
  constructor(private variableService: VariableService) { }

  ngOnInit() {
    this.variables = this.variableService.getVariables();
    console.log(this.variables);
    this.varsLength = this.variables.length;
    this.limit = 5;
  }
  filterVariables(filter: string) {
    this.variables = this.variableService.getVariables(filter);
    console.log(filter);
  }
  loadMoreVars() {
      this.limit = this.varsLength;
  }
}
