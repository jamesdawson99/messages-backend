import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariableLinkComponent } from './variable-link.component';

describe('VariableLinkComponent', () => {
  let component: VariableLinkComponent;
  let fixture: ComponentFixture<VariableLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariableLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
