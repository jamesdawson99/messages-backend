import { Variable } from './../../../../shared/models/variable';
import { VariableService } from './../../variables.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-variable-link',
  templateUrl: './variable-link.component.html',
  styleUrls: ['./variable-link.component.scss']
})
export class VariableLinkComponent implements OnInit {

  selectedVariable: Variable;
  @Input() index: number;
  @Input() variable: Variable;
  constructor(private variableService: VariableService) { }

    ngOnInit() {
    }

    pickVariable() {
      this.variableService.setVariableString(this.variable.var_string);

    }
}
