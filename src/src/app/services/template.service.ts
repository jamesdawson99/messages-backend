import { User } from './../shared/models/user';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/debounceTime';

import { Template } from '../shared/models/template';

@Injectable()
export class TemplateService {

  user: User;
  private apiUrl: string = environment.apiUrlV1;
  private templateUrl = `${this.apiUrl}/api`;
  private lambdaUrl = 'https://0pz91xvz2j.execute-api.eu-west-1.amazonaws.com/dev/ngt/';
  constructor(private http: HttpClient) {
    this.user = {
      username: '',
      _id: '',
      password: '',
      firstName: '',
      lastName: '',
      roles: [],
      skills: [],
      active: true
    };
   }

  createTemplate(name: string, description: string, author: string, user_id: string, body: string, singleton: boolean, freetype: boolean, from: string): Observable<any> {
    return this.http.post(this.templateUrl, { name, description, author, user_id, body, singleton, freetype, from }).map(response => {
      return response;
    });
  }
  updateTemplate(id: string, name: string, description: string, author: string, user_id: string, body: string, singleton: boolean, freetype: boolean, from: string): Observable<any> {
    return this.http.post(`${this.templateUrl}/` + id, { name, description, author, user_id, body, singleton, freetype, from }).map(response => {
      return response;
    });
  }
  getTemplates(state: string, page: number): Observable<any> {
    return this.http.get(`${this.templateUrl}/${state}/all/${page}`).map(response => {
      return response;
    });
  }
  getUser(): Observable<any> {
    return this.http.get(`${this.templateUrl}/heartbeat`, {observe: 'response'}).map(response => {
      this.user = JSON.parse(response.headers.get('X-Oauth-User'));
      return response;
    });
  }
  countTemplate(state: string): Observable<any> {
    return this.http.get(`${this.templateUrl}/count/${state}`).map(response => {
      return response;
    });
  }
  getTemplate(id: string) {
    return this.http.get(`${this.templateUrl}/${id}`).map(response => {
      return response;
    });
  }
  archiveTemplate(id: string): Observable<any> {
    return this.http.put(`${this.templateUrl}/${id}/archive`, {}).map(response => {
      return response;
    });
  }
  interpolateTemplate(string: string, variables: any) {
    return this.http.post(`${this.lambdaUrl}`, { string, variables }) .map(response => {
      return response;
    }).debounceTime(100);
  }
  duplicateTemplate(id: string): Observable<any> {
    return this.http.get(`${this.templateUrl}/duplicate/` + id, {}).map(response => {
      return response;
    });
  }
}
