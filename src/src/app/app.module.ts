import { CKEditorModule } from 'ng2-ckeditor';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { HttpClientModule } from '@angular/common/http';
import { SearchFilterPipe } from './shared/pipes/search.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HeaderModule} from '@glabs/component-library';
import { ToastyModule } from 'ng2-toasty';
import { LoadingModule } from 'ngx-loading';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SafeHtmlPipe } from './shared/pipes/safeHtml.pipe';
import { TemplateService } from './services/template.service';
import { VariableService } from './components/message/variables.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { VariablesComponent } from './components/message/variables/variables.component';
import { VariableLinkComponent } from './components/message/variables/variable-link/variable-link.component';
import { TemplateFilterComponent } from './components/templates/template-filter/template-filter.component';
import { TemplateTableComponent } from './components/templates/template-table/template-table.component';
import { MessageComponent } from './components/message/message.component';
import { MessageCreateComponent } from './components/message/message-create/message-create.component';
import { MessageDataComponent } from './components/message/message-data/message-data.component';
import { MessageInputComponent } from './components/message/message-input/message-input.component';
import { MessagePreviewComponent } from './components/message/message-preview/message-preview.component';
import { MessageVersionComponent } from './components/message/message-version/message-version.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,

    TemplatesComponent,
    HeaderComponent,
    PageNotFoundComponent,
    VariablesComponent,
    VariableLinkComponent,
    SearchFilterPipe,
    TemplateFilterComponent,
    TemplateTableComponent,
    SafeHtmlPipe,
    MessageComponent,
    MessageCreateComponent,
    MessageDataComponent,
    MessageInputComponent,
    MessagePreviewComponent,
    MessageVersionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    HeaderModule,
    NgxPaginationModule,
    LoadingModule,
    ToastyModule.forRoot(),
    SweetAlert2Module.forRoot({
      customClass: 'modal-content',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-blue',
      cancelButtonClass: 'btn'
    }),
    CKEditorModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [
    VariableService,
    TemplateService,
    {
     provide: PERFECT_SCROLLBAR_CONFIG,
     useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
     }
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
