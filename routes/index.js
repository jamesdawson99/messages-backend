const scriptRoutesV1 = require('./v1/script');

const routes = (app) => {
    app.use('/api', scriptRoutesV1);
};

module.exports = routes;

// http://localhost:8000/api/v1/sms POST
