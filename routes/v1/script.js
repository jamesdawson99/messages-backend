const Router = require('express').Router();

const Script = require('./../../controllers/script');

Router.route('/').post(Script.create);
Router.route('/duplicate/:id').get(Script.duplicate);
Router.route('/heartbeat').get(Script.heartbeat);
Router.route('/count/:filter').get(Script.countAll);
Router.route('/:id').post(Script.update);
Router.route('/:filter/all/:page').get(Script.getAll);
Router.route('/:id').get(Script.getOne);
Router.route('/:id/archive').put(Script.archive);
module.exports = Router;
