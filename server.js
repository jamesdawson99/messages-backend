// require('./config/config');
require('./database/db');

const express = require('express');
const path = require('path');
const loggerMorgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const helmet = require('helmet');
const logger = require('@glabs/glabs-logger');
const cors = require('cors');
const compression = require('compression');
const rateLimit = require('express-rate-limit');


const port = process.env.PORT || 8000;

let app = express();

let limiter = new rateLimit({
  windowMs: 15*60*1000, // 15 minutes
  max: 50, // limit each IP to 100 requests per windowMs
  delayMs: 0 // disable delaying - full speed until the max limit is reached
});

app.use(helmet());
app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }));
app.use(bodyParser.urlencoded({ limit: '50mb' ,'extended': true }));
app.use(cookieParser());
app.use(methodOverride());
app.use(compression());


if (process.env.NODE_ENV !== 'test') {
  app.use(loggerMorgan('dev'));
  // app.use(limiter);
}

if (process.env.NODE_ENV === 'development') {
  corsOptions = {
    allowedHeaders: ['x-oauth-user', 'Content-Type'], 
    exposedHeaders: ['x-oauth-user','Content-Type']  
   }
  function corsOptionsDelegate(req,cb){
    cb(null,corsOptions)
  }
  app.use(cors(corsOptionsDelegate)); // dev only

  function addFakeHeaders(req,res,next){
    req.headers["x-oauth-user"] = JSON.stringify({
      username: "buddy",
      _id : "yep",
      firstName : "John",
      lastName : "Smith",
      password : "password11",
      active : true,
      roles: [],
      skills: []
    });
    next();
  }
  
  app.use(addFakeHeaders);
}

// Always need to create response headers.
function createResponseHeaders(req, res, next) {
  if (req.headers && req.headers['x-oauth-user']) { // Heartbeat will not have these headers so we need to check.
    logger.debug('Header user received backend:', req.headers['x-oauth-user']);
    res.setHeader('x-oauth-user', req.headers['x-oauth-user']);
    if (res.getHeader('x-oauth-user')) {
      logger.debug('Header user just after we set it:', res.getHeader('x-oauth-user'));
    } else {
      logger.debug('No oauth user found after we set it');
    }
  }
  next();
}
app.use(createResponseHeaders);

require('./routes')(app);

app.use('/apidocs', express.static(__dirname + '/apidoc'));

if (process.env.NODE_ENV === 'production') {
  app.use('/assets/img', express.static(path.join(__dirname, 'src', 'assets')));
  app.use('/', express.static(path.join(__dirname, 'src', 'dist')));
  // If we get to here and have not found what we're looking for then we must be at a client side route and need to serve the index.html.
  app.get('/*', (req, res) => { 
    res.sendFile(path.resolve(__dirname, 'src', 'dist', 'index.html'));
  }); 
}

app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});

module.exports = { app };
