const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const timestamps = require('mongoose-timestamps');
const paginate = require('mongoose-paginate');

const Schema = mongoose.Schema;
const SchemaTypes = Schema.Types;

const SettingsSchema = new Schema({

    mobile: {
        type: String,
        required: false
    }


})

SettingsSchema.plugin(uniqueValidator);
SettingsSchema.plugin(timestamps);
SettingsSchema.plugin(paginate);

const Settings = mongoose.model('Settings', SettingsSchema);

module.exports = { Settings };
