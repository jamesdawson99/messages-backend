const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const timestamps = require('mongoose-timestamps');
const paginate = require('mongoose-paginate');

const Schema = mongoose.Schema;
const SchemaTypes = Schema.Types;

const ScriptSchema = new Schema({
    name: {
        type: String,
        required: 'Name is required',
        minlength: 1,
        maxlength: [100, 'The name must be lower than 100 characters'],
        lowercase: true,
        trim: true
    },

    description: {
        type: String,
        required: false
    },
    author: {
        type: String,
        required: false
    },
    user_id: {
        type: String,
        required: false
    },
    version: {
        type: Number,
        required: false,
        default: 1
    },
    isChild: {
        type: Boolean,
        required: false,
        default: false
    },

    body: {
        type: String,
        required: false,
        minlength: 1
    },
    childrens: [{
        type: SchemaTypes.ObjectId,
        ref: 'Script_templates'
    }],

    archived: {
        type: Boolean,
        required: false,
        default: false
    }
});

ScriptSchema.plugin(uniqueValidator);
ScriptSchema.plugin(timestamps);
ScriptSchema.plugin(paginate);

const Script = mongoose.model('Script_templates', ScriptSchema);

module.exports = { Script };
