const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamps');


const Schema = mongoose.Schema;

const ChildrenSchema = new Schema({
    version: {
        type: Number,
        required: 'Version is required'
    },
    body: {
        type: String,
        required: 'Text is required',
        minlength: 1
    },

    archived: {
        type: Boolean,
        required: false,
        default: false
    }
});

ChildrenSchema.plugin(timestamps);

const Children = mongoose.model('Children', ChildrenSchema);

module.exports = { Children };
